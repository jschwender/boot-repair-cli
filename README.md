boot-repair

CLI command to install grub on a device.
  Kind of boot repair, but does not need GUI.
  Helpful if boot-repair-disk fails due to weird graphics card ot framebuffer problems.
  Installs grub to the partition where the current directory is located on.
  Works with MBR or EFI install, tested on GRML.
  
  The script finds the device of the current directory,
  then finds the mount point of the device,
  then mounts /dev /sys /proc, to the corresponding directories 
  beneath the mount point. This is required to run grub-install.
  Then it chroots into the mount point and runs grub-install.
  
  
  ©2021, Joachim Schwender


see also LIZENZ file
